<?php

/**
 * @file pagespeed.admin.inc
 * Administration menu for the PageSpeed module.
 */

/**
 * Generate the configuration menu
 *
 * @return mixed
 */
function pagespeed_admin_form() {

  $form = array();

  $form['usage'] = array(
    '#type' => 'fieldset',
    '#title' => t('Usage Settings'),
    '#collapsed' => FALSE,
    '#collapsible' => FALSE
  );

  $form['usage']['pagespeed_usage'] = array(
    '#type' => 'radios',
    '#title' => t('Use PageSpeed on specific pages'),
    '#options' => array(
      PAGESPEED_NOT_LISTED => t('All pages except those listed'),
      PAGESPEED_LISTED => t('Only the listed pages'),
    ),
    '#default_value' => variable_get('pagespeed_usage', PAGESPEED_NOT_LISTED)
  );

  $form['usage']['pagespeed_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. " .
      "Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.",
      array(
        '%blog' => 'blog',
        '%blog-wildcard' => 'blog/*',
        '%front' => '<front>'
      )
    ),
    '#default_value' => variable_get('pagespeed_pages', PAGESPEED_PAGES)
  );

  $form['usage']['pagespeed_node_limit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Limit PageSpeed to specific node types'),
    '#default_value' => variable_get('pagespeed_node_limit', PAGESPEED_NODE_LIMIT)
  );

  $node_types = node_type_get_types();
  $node_options = array();
  foreach ($node_types as $key => $node_type) {
    $node_options[$key] = $node_type->name;
  }
  ksort($node_options);
  $form['usage']['pagespeed_nodes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types to allow PageSpeed to run on.'),
    '#options' => $node_options,
    '#default_value' => variable_get('pagespeed_nodes', array()),
    '#states' => array(
      'invisible' => array(
        ':input[name="pagespeed_node_limit"]' => array('checked' => FALSE),
      ),
    )
  );

  return system_settings_form($form);
}